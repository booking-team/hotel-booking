Rails.application.routes.draw do

  resources :administrators


  get '/rooms', to: 'rooms#index'
  resources :reservations, shallow: true do
    resources :rooms
  end
  devise_for :users, controllers: { registrations: 'users/registrations' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root :to => 'home#index'

  namespace :admin do
    resources :reservations
    resources :profiles
  end
end
