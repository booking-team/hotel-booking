class CreateRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :rooms do |t|
      t.integer :kind
      t.decimal :base_price

      t.timestamps
    end
  end
end
