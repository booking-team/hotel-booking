class AddFieldsToReservationsRooms < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations_rooms, :name, :string
    add_column :reservations_rooms, :email, :string
    add_column :reservations_rooms, :number_phone, :integer
  end
end
