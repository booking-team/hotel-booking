class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :last_name
      t.integer :identification
      t.integer :kind

      t.timestamps
    end
  end
end
