class AddStimatedCostToReservation < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :stimated_cost, :decimal
  end
end
