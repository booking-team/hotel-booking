class CreateReservationsRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations_rooms do |t|
      t.references :room
      t.references :reservation
      t.decimal :estimated_cost

      t.timestamps
    end
  end
end
