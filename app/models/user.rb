class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_one :profile
  has_many :reservations
  accepts_nested_attributes_for :profile
  validates :email, presence:{ message:"debes ingresar un email"}
  validates :password, presence:{message: "debes ingresar una clave"}

  def temporal_reservation
    #capturar variables de instancia
    nueva = reservations.new check_in: Date.today, check_out: Date.today
    nueva.save
  end 

  def self.verify_status
    s = 0
    hola = Reservation.where("status = ?", s)
    hola.each do |item|
        item.destroy
      end
      p "hola"
    end


  def profile
    super || build_profile
  end 


end
