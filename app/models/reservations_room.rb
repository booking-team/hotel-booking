class ReservationsRoom < ApplicationRecord
    belongs_to :reservation
    belongs_to :room
    validates :name, presence: true, on: :update
end
