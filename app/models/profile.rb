class Profile < ApplicationRecord
    belongs_to :user
    validates :name,:last_name, presence: :true, format: { with: /\A[a-z A-Z ]+\z/,
        message: "solo se permiten letras" }
    validates :identification, numericality:{message: "en este campo debe ingresar solo números"}

    enum kind: { usuario: 0, admin: 1}

end
