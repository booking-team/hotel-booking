class Reservation < ApplicationRecord
    belongs_to :user
    has_many :reservations_rooms, dependent: :destroy
    has_many :rooms, through: :reservations_rooms
    accepts_nested_attributes_for :reservations_rooms, reject_if: :all_blank
    
    enum kind: { prepending: 0, pending: 1, confirmed: 2}
end

