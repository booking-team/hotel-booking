class ReservationDatatable < ApplicationDatatable
    delegate :admin_reservations_path, to: :@view
  
    private
  
    def data
      reservations.map do |reservation|
        [].tap do |column|
          column << reservation.check_in
          column << reservation.check_out
          column << User.find(reservation.user_id).profile.name
          column << User.find(reservation.user_id).profile.last_name
  
          links = []
          links << link_to('Show', reservation)
          links << link_to('Edit', edit_reservation_path(reservation))
          links << link_to('Destroy', user, method: :delete, data: { confirm: 'Are you sure?' })
          column << links.join(' | ')
        end
      end
    end
  
    def count
      Reservation.count
    end
  
    def total_entries
      reservations.total_count
      # will_paginate
      # users.total_entries
    end
  
    def reservations
      @reservations ||= {
        check_in: Reservation.check_in
        check_out: Reservation.check_out


      }
    end
  
    def fetch_reservations
      search_string = []
      columns.each do |term|
        search_string << "#{term} like :search"
      end
  
      # will_paginate
      # users = User.page(page).per_page(per_page)
      reservations = Reservation.order("#{sort_column} #{sort_direction}")
      reservations = reservations.page(page).per(per_page)
      reservations = reservations.where(search_string.join(' or '), search: "%#{params[:search][:value]}%")
    end
  
    def columns
      %w(first_name last_name email phone_number)
    end
  end