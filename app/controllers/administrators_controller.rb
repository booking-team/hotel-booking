class AdministratorsController < ApplicationController
  def index
    respond_to do |format|
      format.html
      format.json { render json: ReservationsDatatable.new(view_context) }
    end
  end
end
