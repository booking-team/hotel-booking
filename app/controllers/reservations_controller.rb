class ReservationsController < ApplicationController
  before_action :authenticate_user!, only: :create
  def index
    respond_to do |format|
      format.html
      format.json { render json: UsersDatatable.new(view_context) }
    end
  end

  def show
  end

  def destroy
  end

  def new
    @test = Reservation.where("check_out >= ? AND check_in <= ?", params[:fecha], params[:fecha_final])
    @rooms = Room.all
    @temp = [] 
    @test.each do |reservation|
      reservation.rooms.each do |d|
        @temp << d
      end
    end
    @available = (@rooms-@temp).uniq
    @available = @available.delete_if {|c| c.capacity != params[:capacidad].to_i }
    @reservation = Reservation.new user_id: 1
    @reservation.reservations_rooms.new
  end

  def create
    if still_available == 0
      redirect_to new_reservation_path, notice: "ya no esta disponible"
      return 
    end
    @new = Reservation.new reservation_params
    @new.user_id = current_user.id
    @new.status = 1
    @new.save!
    id = current_user.reservations.last.id
    check_time
    redirect_to (edit_reservation_path(id)) and return 
  end

  def edit
    @reservation = Reservation.find_by id: params[:id]
    if @reservation == nil 
      redirect_to new_reservation_path, notice: "borrada" 
      return 
    end
    @reservation.stimated_cost = get_stimated_cost
    @reservation.save!
  end

  def update
    @reservation = Reservation.find(params[:id])
    @reservation.update! reservation_params
    redirect_to reservation_path(current_user.reservations.last)
  end

  def check_time 
    @t = Thread.new do
    @limit = 0
      until @limit == 300
        sleep(1)
        @limit+=1 
        p "check"
      end
      if @limit == 300 && current_user.reservations.last.status == 1
        reservation = current_user.reservations.last
        reservation.status = 0 
        reservation.save!
      end
    end

end

def reservation_params
params.require(:reservation).permit(
  :check_in,
  :check_out,
  :status,
  :stimated_cost,
  reservations_rooms_attributes:[:reservation_id, :room_id,:name,:email,:number_phone])
end 
 
def get_stimated_cost
  total = 0
  @reservation.rooms.each do |room|
    total = total + room.base_price
  end
  total
end 

def still_available 
  aval = []
  params_form = reservation_params
  re_search = Reservation.where("check_out >= ? AND check_in <= ? ", reservation_params["check_in"], reservation_params["check_out"])
  params_form[:reservations_rooms_attributes].each do |room|
    aval << room[1]["room_id"].split("\r\n")
  end
  rooms = []
  re_search.each do |reservation|
    reservation.rooms.each do |room|
      rooms << room.id 
    end
  end
  aval = aval.delete_if {|c| c.empty? }
  aval =  aval.join.chars()
  rooms = rooms.uniq{|x| x}
  rooms_string = rooms.map {|c| c.to_s}
  if (aval & rooms_string).empty? == false 
    available = 0 
  end
  available
end 

end
