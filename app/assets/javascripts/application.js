// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery3
//= require popper
//= require bootstrap
//= require jquery.validate
//= require_tree .





// window.addEventListener("beforeunload",function(e){
//   var confirmationMessage = "\o/";
//   (e || window.event).returnValue = confirmation;
//   return confirmationMessage;
// })


$(document).ready(function () {

  
  })

   

    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
      }, "este campo solo acepta caracteres alfabeticos");

    $.validator.addMethod("limitdate", function(value,element){
      var currentDate = new Date()
      var inputDate = new Date(value)      
      if (inputDate < currentDate){
        return false; 
      }else{
        return true;
      }
    },"fecha no valida")


    jQuery.validator.addMethod("greaterThan", 
    function(value, element, params) { 
      return new Date(value) > new Date($(params).val());
    },'debe ser mayor');

    jQuery.validator.addMethod("lesserThan", 
    function(value, element, params) { 
      return new Date(value) < new Date($(params).val());
    },'debe ser menor');
  

    $("#new_search").validate({
      rules: {
        fecha: {
          required: true,
          date: true,
          limitdate: true
        },
        
        fecha_final: {
          required: true,
          date: true,
          limitdate: true,
          greaterThan: "#fecha"
        }
      },

      "messages":{
        fecha:{
          required: "este campo es requerido"
        }
      }
    })

    $("#new_user").validate({
    rules: {
    "user[email]": {required: true, email: true},
    "user[password]": {required: true, minlength: 6},
    "user[password_confirmation]": {required: true, equalTo: "#user_password"},
    "user[profile_attributes][name]": {required: true, lettersonly: true},
    "user[profile_attributes][last_name]": {required: true, lettersonly: true},
    "user[profile_attributes][identification]": {required: true, number: true}
    },

    "messages":{
        "user[email]": {
            required: "por favor ingresa un email",
            email: "ingresa un formato valido de email"
        },

        "user[password]": {
            required: "por favor ingresa un clave",
            minlength: "la clave debe tener minimo 6 caracteres"
        },
        "user[password_confirmation]": {
            required: "por favor confirma tu clave",
            minlength: "la clave debe tener minimo 6 caracteres",
            equalTo: "debe ser igual al primer campo"
        },
        "user[profile_attributes][name]": {
            required: "por favor ingresa tu nombre"
        },
        "user[profile_attributes][last_name]": {
            required: "por favor ingresa tu apellido",
        },

        "user[profile_attributes][identification]": {
            required: "por favor ingresa tu identificación",
            number: "solo puedes ingresar números"
        }
    }
    });


    document.addEventListener('turbolinks:load',function(){
      
      var sections = $('section')
      , nav = $('nav')
      , navheight = nav.outerHeight();
      nav.find('a').addClass('active-white')

    $(window).on('scroll', function(){
      var current_pos = $(this).scrollTop();
      sections.each(function(){
        var top = $(this).offset().top - navheight,
        bottom = top + $(this).outerHeight()
        if(current_pos >= top && current_pos <= bottom){
          if ($(this).attr('id')!=1){
            nav.addClass('bg-light')
            nav.find('a').removeClass('active-white')
            nav.find('a').addClass('active-black')
            nav.find('a').removeClass('active-link')
            nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active-link')
          }else{
            nav.removeClass('bg-light')
            nav.find('a').removeClass('active-link')
            nav.find('a').removeClass('active-black')
            nav.find('a').addClass('active-white')
          }
        }
      }) 
    })

    nav.find('a').on('click', function(){
      var $n = $(this)
      , m = $n.attr('href')
      $('html, body').animate({
        scrollTop: $(m).offset().top - navheight 
      }, 500)
      return false
    })

      
      $('#table_id').DataTable({
        "pageLength": 10,
        "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
        
        }})
      })
      
 

  var encendido = 0;
  var encendidoDos = 0;
  var encendidoTres = 0;
  var encendidoCuatro = 0;
  var total = 0;

  var checkBox = document.getElementById("0")
  checkBox.addEventListener('change',function() {

    var precio = document.getElementById("precio0").innerHTML
    var nameRoomOne = document.getElementById("room0").innerHTML
    total = document.getElementById("total")
    if (encendido == 1){
      encendido = encendido - 1
      total.value = Number(total.value) - Number(precio)
      room = document.getElementById("otro");
      tax1 = document.getElementById("tax1");
      room.parentNode.removeChild(room);
      tax1.parentNode.removeChild(tax1)
      var tokyo = document.getElementById("deluxe2")
      tokyo.value = ""
    }else{
      console.log('On!')
      encendido = encendido + 1
      precio.value = Number(precio)
      total.value = Number(total.value) + Number(precio)
      var input = document.createElement("DIV");
      var taxes = document.createElement("DIV")
      var price  = document.getElementById("taxes")
      taxes.innerHTML = (Number(precio)*10)/100
      taxes.id = "tax1"
      input.innerHTML = nameRoomOne
      input.id = "otro"
      price.appendChild(taxes)
      list.appendChild(input);
      var tokyo = document.getElementById("deluxe2")
      tokyo.value = document.getElementById("id0").innerText
    }
  });

  var checkBox2 = document.getElementById("1")
  checkBox2.addEventListener('change',function() {

    var precioRoomDos = document.getElementById("precio1").innerHTML
    var nameRoomTwo = document.getElementById("room1").innerHTML
    total = document.getElementById("total")


    if (encendidoDos == 1){
      encendidoDos = encendidoDos - 1
      total.value = Number(total.value) - Number(precioRoomDos)
      tax2 = document.getElementById("tax2")
      room2 = document.getElementById("caracas");   
      room2.parentNode.removeChild(room2);
      tax2.parentNode.removeChild(tax2)
      var tokyo = document.getElementById("caracas2")
      tokyo.value = ""

    }else{
      encendidoDos = encendidoDos + 1
      total.value = Number(total.value) + Number(precioRoomDos)
      var inputTwo = document.createElement("DIV");
      var taxes = document.createElement("DIV")
      var price  = document.getElementById("taxes")
      taxes.innerHTML = (Number(precioRoomDos)*10)/100
      taxes.id = "tax2"
      inputTwo.innerHTML = nameRoomTwo
      inputTwo.id = "caracas"
      var list2 = document.getElementById("list");
      list2.appendChild(inputTwo);
      price.appendChild(taxes)
      var tokyo = document.getElementById("caracas2")
      tokyo.value = document.getElementById("id1").innerText
    }
  });

  var checkBox3 = document.getElementById("2")
  checkBox3.addEventListener('change',function() {
    var priceRoomTree = document.getElementById("precio2").innerHTML
    var nameRoomTree = document.getElementById("room2").innerHTML
    total = document.getElementById("total")
    var list3 = document.getElementById("list");
    var price = document.getElementById("taxes")


    if (encendidoTres == 1){

      encendidoTres = encendidoTres - 1
      total.value = Number(total.value) - Number(priceRoomTree)
      roomTree = document.getElementById("viena")
      tax3 = document.getElementById("tax3")
      roomTree.parentNode.removeChild(roomTree);
      tax3.parentNode.removeChild(tax3)
      var tokyo = document.getElementById("viena2")
      tokyo.value = ""
    }else{
      encendidoTres = encendidoTres + 1
      total.value = Number(total.value) + Number(priceRoomTree)
      var inputTree = document.createElement("DIV");
      tax3 = document.createElement("DIV")
      tax3.innerHTML = Number(priceRoomTree)*10/100
      tax3.id = "tax3"
      inputTree.innerHTML = nameRoomTree
      inputTree.id = "viena"
      list3.appendChild(inputTree) 
      price.appendChild(tax3)
      var tokyo = document.getElementById("viena2")
      tokyo.value = document.getElementById("id2").innerText
    }
  });

  var checkBox4 = document.getElementById("3");
  checkBox4.addEventListener('change',function() {

    var priceRoomFour = document.getElementById("precio3").innerHTML
    var nameRoomFour = document.getElementById("room3").innerHTML
    total = document.getElementById("total")
    var list4 = document.getElementById("list");
    var taxes = document.getElementById("taxes")

    if (encendidoCuatro == 1){

      encendidoCuatro = encendidoCuatro - 1
      total.value = Number(total.value) - Number(priceRoomFour)
      roomFour = document.getElementById("tokyo")
      tax4 = document.getElementById("tax4")
      tax4.parentNode.removeChild(tax4)
      roomFour.parentNode.removeChild(roomFour);
      var tokyo = document.getElementById("tokyo2")
      tokyo.value = ""
    }else{
      encendidoCuatro = encendidoCuatro + 1
      total.value = Number(total.value) + Number(priceRoomFour)
      var inputFour = document.createElement("DIV");
      var tax = document.createElement("DIV")
      tax.innerHTML = Number(priceRoomFour)*10/100
      tax.id = "tax4"
      inputFour.innerHTML = nameRoomFour
      inputFour.id = "tokyo"
      list4.appendChild(inputFour)
      taxes.appendChild(tax)
      var tokyo = document.getElementById("tokyo2")
      tokyo.value = document.getElementById("id3").innerText
    }
  });






